﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Net;
using System.Xml;
using System.Web.UI.WebControls;

public partial class Segment_UI : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        string segmentID = Session["SegmentID"].ToString();
        string prefill = Session["Prefill"].ToString();
        string user = Session["User"].ToString();

        Session["Segment"] = null;
        Session["Prefill"] = null;
        Session["User"] = null;

        ProcessInstruction(segmentID, prefill, user);
    }
    private string ProcessInstruction(string xID, string xPrefill, string user)
    {
        try
        {
            //add client functionality to Word, which will allow users to select segment and prefill, then send to server
            TSG.ForteServer.ForteServerClient oServer = new TSG.ForteServer.ForteServerClient();

            //string xPrefill2 = "<IncludeTitle>true</IncludeTitle><Recipients><FULLNAMEWITHPREFIXANDSUFFIX Index="1" UNID="0">Jeffrey</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index="1" UNID="0">Developer</TITLE><COMPANY Index="1" UNID="0">TSG</COMPANY><COREADDRESS Index="1" UNID="0">my address&#13;&#10;my city, my state my zip</COREADDRESS><FULLNAMEWITHPREFIXANDSUFFIX Index="2" UNID="0">Valerie Melville</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index="2" UNID="0">Manager</TITLE><COMPANY Index="2" UNID="0">TSG</COMPANY><COREADDRESS Index="2" UNID="0">her address&#13;&#10;her city, her state her zip</COREADDRESS><CIDetailTokenString>[CIDetail__FullNameWithPrefixAndSuffix][CIDetail__TitleIfBusiness][CIDetail__CompanyIfBusiness][CIDetail__COREADDRESS]</CIDetailTokenString></Recipients><UseTableFormat>false</UseTableFormat><Reline><Standard Format="0" Default="Alternate Insurance">this is the reline</Standard><Special Format="0" Default="Alternate Insurance"><Label Index="1">Claimant</Label><Value Index="1">a</Value><Label Index="2">Date of Claim</Label><Value Index="2">b</Value><Label Index="3">Company Name</Label><Value Index="3">c</Value><Label Index="4">Action Number</Label><Value Index="4">d</Value></Special></Reline><Subject>This is the subject line</Subject><Agreed>true</Agreed><DateFormat>MMMM dd, yyyy</DateFormat><ExecuteonBlock>true</ExecuteonBlock><TitleProperty>doc title</TitleProperty><CustomTitle>custom doc title</CustomTitle><DocumentVariableValue>dv custom 1 var text</DocumentVariableValue><SetLanguage>true</SetLanguage><FontName>Calibri</FontName><FontSize>12.0</FontSize><BodyTextFirstLineIndent>1.0</BodyTextFirstLineIndent><BodyTextAlignment>0</BodyTextAlignment><AuthorName>Daniel C. Fisherman</AuthorName><Culture>1033</Culture><Authors>9358.0¦-1¦Daniel C. Fisherman¦7659872|</Authors>";
            //string xPrefill = null;

            xPrefill = "<Prefill>" + xPrefill + "</Prefill>";
            xPrefill = ConvertXmlToDelimitedString(xPrefill);

            string strHostName = Dns.GetHostName();
            Console.WriteLine("Local Machine's Host Name: " + strHostName);
            // Then using host name, get the IP address list..
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            string addr = "unknown";

            foreach (var ip in ipEntry.AddressList)
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork && ip.ToString().StartsWith("10."))
                {
                    addr = strHostName + " (IP: " + ip.ToString() + "; Doc ID: " + xID + ")";
                    break;
                }
            }

            string instruction = null;

            //switch (this.lstRequest.Text.ToLower())
            //{
            //    case "createdocument":
            //        instruction = "<ForteInstructions><ForteInstruction type=\"CreateDocument\"><User>" + user + "</User><ID>" + this.txtArg1.Text + "</ID><Prefill>" + xPrefill +
            //     "</Prefill><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
            //        break;
            //    case "getfoldersxml":
            //        instruction = "<ForteInstructions><ForteInstruction type=\"GetFoldersXml\"><User>" + user + "</User><ParentFolderID>" + this.txtArg1.Text +
            //            "</ParentFolderID><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
            //        break;
            //    case "getfoldermembersjson":
            //        instruction = "<ForteInstructions><ForteInstruction type=\"GetFoldersXml\"><User>" + user + "</User><ParentFolderID>" + this.txtArg1.Text +
            //            "</ParentFolderID><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
            //        break;
            //    case "getuixml":
            //        instruction = "<ForteInstructions><ForteInstruction type=\"GetUIXml\"><User>" + user + "</User><ID>" + this.txtArg1.Text + "</ID><Prefill>" + xPrefill +
            //     "</Prefill><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
            //        break;
            //    case "getuihtml":
            //    case "getuihtml (native)":
            //        instruction = "<ForteInstructions><ForteInstruction type=\"GetUIHtml\"><User>" + user + "</User><ID>" + this.txtArg1.Text + "</ID><Prefill>" + xPrefill +
            //     "</Prefill><Format>" + "0" + "</Format><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
            //        break;
            //    case "getuihtml (jstree)":
            //        instruction = "<ForteInstructions><ForteInstruction type=\"GetUIHtml\"><User>" + user + "</User><ID>" + this.txtArg1.Text + "</ID><Prefill>" + xPrefill +
            //     "</Prefill><Format>" + "1" + "</Format><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
            //        break;
            //    case "getuihtml (fancytree)":
            instruction = "<ForteInstructions><ForteInstruction type=\"GetUIHtml\"><User>" + user + "</User><ID>" + xID + "</ID><Prefill>" + xPrefill +
         "</Prefill><Format>" + "0" + "</Format><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
            //        break;
            //    case "getlistxml":
            //        instruction = "<ForteInstructions><ForteInstruction type=\"GetListXml\"><User>" + user + "</User><ListName>" + this.txtArg1.Text + "</ListName>" +
            //     "<EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
            //        break;
            //    case "gethelptext":
            //        string xHelp = "";
            //        if (this.txtArg1.Text != "")
            //        {
            //            if (xHelp.IndexOf(@"file:///") == -1 && xHelp.IndexOf(@"file:\\\") == -1)
            //            {
            //                xHelp = @"file:///" + this.txtArg1.Text;
            //            }
            //            else
            //            {
            //                xHelp = this.txtArg1.Text;
            //            }
            //        }
            //        else
            //        {
            //            xHelp = this.txtArg2.Text;
            //        }
            //        instruction = "<ForteInstructions><ForteInstruction type=\"GetHelpText\"><User>" + user + "</User><HelpText>" + xHelp + "</HelpText>" +
            //     "<EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
            //        break;
            //    case "getexternaldataset":
            //        instruction = "<ForteInstructions><ForteInstruction type=\"GetExternalDataSet\"><User>" + user + "</User><ID>" + this.txtArg1.Text +
            //            "</ID><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
            //        break;
            //    default:
            //        MessageBox.Show("Support for this request has yet to be implemented.  Coders, see Form1.ProcessInstruction().",
            //            "Not implemented", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            //        return null;
            //}

            string[] aText = oServer.ProcessInstructionSet(instruction);

            string xText = string.Join("\r\n\r\n", aText);

            System.IO.StringWriter writer = new System.IO.StringWriter();
            HttpContext.Current.Response.Write(xText);
            return null;
        }
        catch (System.Exception oE)
        {
            throw oE;
        }
    }

    private string ConvertXmlToDelimitedString(string xPrefill)
    {
        StringBuilder oSB = new StringBuilder();

        XmlDocument oXml = new XmlDocument();
        oXml.LoadXml(xPrefill);

        XmlNodeList oElements = oXml.DocumentElement.ChildNodes;
        foreach (XmlNode element in oElements)
        {
            oSB.AppendFormat("{0}¤{1}Þ", element.Name, element.InnerXml);
        }

        oSB.Remove(oSB.Length - 1, 1);
        return oSB.ToString();
    }

}