﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ForteTestWebClient.Startup))]
namespace ForteTestWebClient
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
