﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net;
using System.Text;
using System.Xml;
using System.Web.UI.WebControls;

public partial class _Default : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click1(object sender, EventArgs e)
    {
        Session["Prefill"] = "<DeliveryPhrases>Confidential</DeliveryPhrases><Recipients><FULLNAMEWITHPREFIXANDSUFFIX Index =\"1\" UNID=\"0\">Doug Miller</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index=\"1\" UNID=\"0\">Developer</TITLE><COMPANY Index=\"1\" UNID=\"0\">TSG</COMPANY><COREADDRESS Index=\"1\" UNID=\"0\">28 6th Ave. &#13;&#10;NY, NY 10003</COREADDRESS><FULLNAMEWITHPREFIXANDSUFFIX Index=\"2\" UNID=\"0\">Linda Sackett</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index=\"2\" UNID=\"0\">Operations Manager</TITLE><COMPANY Index=\"2\" UNID=\"0\">TSG</COMPANY><COREADDRESS Index=\"2\" UNID=\"0\">Her address &#13;&#10;Her city, state, and zip</COREADDRESS><CIDetailTokenString>[CIDetail__FullNameWithPrefixAndSuffix][CIDetail__TitleIfBusiness][CIDetail__CompanyIfBusiness][CIDetail__COREADDRESS]</CIDetailTokenString><NewVariable1>900 Fifth Avenue, Suite 2500&#13;&#10;New York, New York  10003</NewVariable1></Recipients><NewVariable2>this is the variable 2 value</NewVariable2><Reline><Standard Format=\"0\" Default=\"Alternate Insurance\">This is the reline value</Standard><Special Format=\"0\" Default=\"Alternate Insurance\"><Label Index=\"1\">Claimant</Label><Value Index=\"1\">c</Value><Label Index=\"2\">Date of Claim</Label><Value Index=\"2\">d</Value><Label Index=\"3\">Company Name</Label><Value Index=\"3\">c</Value><Label Index=\"4\">Action Number</Label><Value Index=\"4\">a</Value></Special></Reline><AuthorInitials>DCF</AuthorInitials><ReLine2><Standard Format=\"0\" Default=\"Alternate Insurance\">reline 2  value</Standard></ReLine2><Culture>1033</Culture><Authors>9358.0¦-1¦Daniel C. Fisherman¦7659872|</Authors>";
        Session["SegmentID"] = "3039";
        Session["User"] = System.Environment.UserName;
        Response.Redirect("SegmentUI.aspx");
    }
}